const bcrypt = require('bcrypt');
const axios = require("axios")
const to = require('await-to-js').default;

const {getUserByEmail} = require('../../query/auth')
const {isError, getSelectResult} = require('../../utils/graphql')
const {getToken} = require('../../utils/auth')

const signIn = async (email, password, res) => {
  
  const [err,userRes] = await to(axios({
    url: process.env.HASURA_ENDPOINT,
    method: 'post',
    headers: {
      'x-hasura-admin-secret': process.env.ADMIN_SECRET
    },
    data: {
      query: getUserByEmail(email)
    }
  }))
  
  if (err || isError(userRes)){
    res.status(401).json({message: "User not found"})
  } else {  
    const user = getSelectResult(userRes, "users")[0]

    const match = await bcrypt.compare(password, user.password);

    if(!match) {
      res.status(401).json({message: "Password incorrect"})
    } else {
      const token = await getToken(user)
      res.status(200).json({
        token,
        message: "Sign in successfully"
      })
    }
  }
}

module.exports = {
    signIn
}