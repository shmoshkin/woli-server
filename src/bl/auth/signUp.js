const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const axios = require("axios");
const to = require('await-to-js').default;

const {createUser} = require('../../query/auth');
const {isError, getInsertResult} = require('../../utils/graphql');

const signUp = async (email, password, firstName, lastName, res) => {

    const [err, hashPassword] = await to(bcrypt.hash(password, 16));    

    if(err) {
        res.status(500).json({ message: 'Error on creating password' })
    } else {
        const [err, user] = await to(axios({
            url: process.env.HASURA_ENDPOINT,
            method: 'post',
            headers: {
                'x-hasura-admin-secret': process.env.ADMIN_SECRET
            },
            data: {
              query: createUser(email, hashPassword, firstName, lastName)
            }
          }))
        if(err || isError(user)) {
            res.status(500).json({ message: 'Error on creating user' })
        } else {
            const userAfterCreation = getInsertResult(user, "insert_users")[0]
            res.status(200).json({ message: `${userAfterCreation.firstName} ${userAfterCreation.lastName}, your user created succussfuly`})
        }
    }
}

module.exports = {
    signUp
}