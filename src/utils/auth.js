const R = require('ramda');
const jwt = require('jsonwebtoken');

const getToken = async (user)  => {

    const payload =  {
      "name": `${user.firstName} ${user.lastName}`,
      "email": user.email,
      "admin": user.admin,
      "role": user.admin ? "admin" : user.role,
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": ["admin","user","anonymous"],
        "x-hasura-default-role": "anonymous",
        "x-hasura-role": user.admin ? "admin" : user.role,
        "x-hasura-user-id": user.email,
        "x-hasura-custom": "group",
      }
    }
      
    const token = await jwt.sign(
      payload,    
      process.env.SECRET,
      { 
        algorithm: 'HS256',
        expiresIn: '356 d'
      }
    )
    
  return token;
}

module.exports = {
  getToken
}