const authRouter = require('express').Router();
const {signUp} = require('../bl/auth/signUp');
const {signIn} = require('../bl/auth/signIn');


authRouter.post('/signIn/:email/:password', function (req, res) {
    const params = req.params;
    signIn(params.email, params.password, res)

})

authRouter.post('/signUp/:email/:password/:firstName/:lastName', function (req, res) {
    const params = req.params;
    signUp(params.email, params.password, params.firstName, params.lastName, res)
})

authRouter.get('/signUp/:email/:password/:firstName/:lastName', function (req, res) {
    res.send('work')
})

authRouter.get('/check', (req, res) => res.send('work'))

module.exports = authRouter