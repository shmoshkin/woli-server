const createUser = (email, password, firstName, lastName) => `mutation {
    insert_users(objects: {email: "${email}", firstName: "${firstName}", lastName: "${lastName}", password: "${password}"}) {
      returning {
        created_at
        email
        firstName
        lastName
      }
    }
  }`

const getUserByEmail = (email) => `query getUserByEmail {
  users(where: {email: {_eq: "${email}"}}) {
    admin
    created_at
    email
    firstName
    lastName
    role
    password
  }
}`

module.exports = {
    createUser,
    getUserByEmail
}