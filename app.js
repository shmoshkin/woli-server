require('dotenv').config()
const logger = require('morgan')
const  bodyParser = require('body-parser');
const express = require('express')
const cors = require('cors')

const app = express()

// General
app.use(cors({origin: process.env.CORS}))
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger())

// Routers
app.use('/auth', require('./src/routers/auth'))

app.listen(process.env.PORT, () => console.log(`Example app listening at http://localhost:${process.env.PORT}`))